//
//  MapLayers.swift
//  SwissMapDemo
//
//  Created by Pranav Panchal on 19/11/20.
//

import MapKit
import UIKit

class MapLayers: NSObject {
    var longitude = 8.2275
    var latitude = 46.8182

    var kml_overlays: [MKOverlay] = []

    var greyColor = UIColor(red: 209.0 / 255.0, green: 209.0 / 255.0, blue: 209.0 / 255.0, alpha: 1)
    var greenColor = UIColor(red: 130 / 255.0, green: 190.0 / 255.0, blue: 0 / 255.0, alpha: 1)

    func InitialMapLoading(mapView: MKMapView) {
        if #available(iOS 13.0, *) {
            addSwissLatLong(mapView: mapView)
        } else {
            // Fallback on earlier versions
        }
        loadKml(mapView: mapView)
        // addOverlays(mapView: mapView)
    }

    // 1. Add Switzerland Latlong and zoom level
    @available(iOS 13.0, *)
    func addSwissLatLong(mapView: MKMapView) {
        let swissCenter = CLLocation(latitude: 46.8182, longitude: 8.2275)
        let region = MKCoordinateRegion(
            center: swissCenter.coordinate,
            latitudinalMeters: 200000,
            longitudinalMeters: 250000)

        mapView.setCameraBoundary(
            MKMapView.CameraBoundary(coordinateRegion: region),
            animated: true)

        let zoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance: 550000)
        mapView.setCameraZoomRange(zoomRange, animated: true)
        mapView.centerCoordinate = region.center
    }

    // 2. Add KML Layer
    fileprivate func loadKml(mapView: MKMapView) {
        let url = Bundle.main.url(forResource: "KML_Sample", withExtension: "kml")
        KMLDocument.parse(url: url!, callback: { [unowned self] kml in
            // Add overlays
            mapView.addOverlays(kml.overlays)
            kml_overlays = kml.overlays

            // Add annotations
            // addAnnotations()
        }
        )
    }

    // 3. Add White layer
    func addOverlays(mapView: MKMapView) {
        if let fullRadius = CLLocationDistance(exactly: MKMapRect.world.size.height) {
            mapView.addOverlay(MKCircle(center: mapView.centerCoordinate, radius: fullRadius))
        }
    }

    // 4. Add Style layer
    func addStyleToMapView(mapView: MKMapView) {
        // We first need to have the path of the overlay configuration JSON
        guard let overlayFileURLString = Bundle.main.path(forResource: "Styler", ofType: "json") else {
            return
        }
        let overlayFileURL = URL(fileURLWithPath: overlayFileURLString)

        // After that, you can create the tile overlay using MapKitGoogleStyler
        guard let tileOverlay = try? MapKitGoogleStyler.buildOverlay(with: overlayFileURL) else {
            return
        }

        // And finally add it to your MKMapView
        mapView.addOverlay(tileOverlay)
    }

    // 5. Remove All layers
    func removeAllMapLayers(mapView: MKMapView) {
        for layers in mapView.overlays(in: .aboveLabels) {
            mapView.removeOverlay(layers)
        }
    }
}

import XCTest

import KlaraSPMTests

var tests = [XCTestCaseEntry]()
tests += KlaraSPMTests.allTests()
XCTMain(tests)
